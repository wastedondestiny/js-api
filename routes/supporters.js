const { setEnvValue, getEnvValue } = require('../utils/env')
const express = require('express')
const { got } = require('got-cjs')
const { getSupporters, createSupporter, updateSupporter } = require('../database/requests')
const { getAccountsForMembershipId } = require('../utils/accounts')
const { getSupporterModels } = require('../utils/factories')

const router = express.Router()

router.get('/', async (req, res) => {
  try {
    const supporters = await getSupporters(req.database)
    return res.json(getSupporterModels(supporters))
  } catch (err) {
    return res.json([])
  }
})

router.get('/patreon/:code', async (req, res) => {
  try {
    const jwt = await got('https://www.patreon.com/api/oauth2/token', {
      method: 'POST',
      searchParams: {
        code: req.params.code,
        grant_type: 'authorization_code',
        client_id: process.env.PATREON_CLIENT_ID,
        client_secret: process.env.PATREON_CLIENT_SECRET,
        redirect_uri: 'https://wastedondestiny.com/patreon'
      }
    }).json()
    const result = await got('https://www.patreon.com/api/oauth2/api/current_user', {
      headers: {
        authorization: `Bearer ${jwt.access_token}`
      }
    }).json()
    return res.json({
      access_token: jwt.access_token,
      ...result
    })
  } catch (error) {
    console.log(error.response && error.response.body ? error.response.body : error)
    return res.status(400).end()
  }
})

router.get('/patreon', async (req, res) => {
  try {
    const result = await got('https://www.patreon.com/api/oauth2/api/current_user', {
      headers: {
        authorization: `Bearer ${req.headers.access_token}`
      }
    }).json()
    return res.json({
      access_token: req.headers.access_token,
      ...result
    })
  } catch (error) {
    console.log(error.response && error.response.body ? error.response.body : error)
    return res.status(400).end()
  }
})

router.get('/discord/:code', async (req, res) => {
  try {
    const jwt = await got('https://discord.com/api/oauth2/token', {
      method: 'POST',
      body: new URLSearchParams({
        code: req.params.code,
        grant_type: 'authorization_code',
        client_id: process.env.DISCORD_CLIENT_ID,
        client_secret: process.env.DISCORD_CLIENT_SECRET,
        redirect_uri: 'https://wastedondestiny.com/patreon'
      }).toString(),
      headers: {
        'content-type': 'application/x-www-form-urlencoded'
      }
    }).json()
    const result = await got('https://discord.com/api/users/@me', {
      headers: {
        authorization: `Bearer ${jwt.access_token}`
      }
    }).json()
    return res.json({
      access_token: jwt.access_token,
      ...result
    })
  } catch (error) {
    console.log(error.response && error.response.body ? error.response.body : error)
    return res.status(400).end()
  }
})

router.get('/discord', async (req, res) => {
  try {
    const result = await got('https://discord.com/api/users/@me', {
      headers: {
        authorization: `Bearer ${req.headers.access_token}`
      }
    }).json()
    return res.json({
      access_token: req.headers.access_token,
      ...result
    })
  } catch (error) {
    console.log(error.response && error.response.body ? error.response.body : error)
    return res.status(400).end()
  }
})

router.get('/bungie/:code', async (req, res) => {
  try {
    const jwt = await got('https://www.bungie.net/platform/app/oauth/token/', {
      method: 'POST',
      body: new URLSearchParams({
        code: req.params.code,
        grant_type: 'authorization_code',
        redirect_uri: 'https://wastedondestiny.com/patreon'
      }).toString(),
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        authorization: `Basic ${process.env.BUNGIE_API_SECRET}`
      }
    }).json()
    const result = await got('https://www.bungie.net/Platform/User/GetMembershipsForCurrentUser/', {
      headers: {
        'X-API-Key': process.env.BUNGIE_API_KEY,
        authorization: `Bearer ${jwt.access_token}`
      }
    }).json()
    const accounts = await getAccountsForMembershipId(result.Response.bungieNetUser.membershipId)
    return res.json({
      access_token: jwt.access_token,
      ...result.Response,
      accounts
    })
  } catch (error) {
    console.log(error.response && error.response.body ? error.response.body : error)
    return res.status(400).end()
  }
})

router.get('/bungie', async (req, res) => {
  try {
    const result = await got('https://www.bungie.net/Platform/User/GetMembershipsForCurrentUser/', {
      headers: {
        'X-API-Key': process.env.BUNGIE_API_KEY,
        authorization: `Bearer ${req.headers.access_token}`
      }
    }).json()
    const accounts = await getAccountsForMembershipId(result.Response.bungieNetUser.membershipId)
    return res.json({
      access_token: req.headers.access_token,
      ...result.Response,
      accounts
    })
  } catch (error) {
    console.log(error.response && error.response.body ? error.response.body : error)
    return res.status(400).end()
  }
})

router.post('/', async (req, res) => {
  try {
    let discordId = ''
    let linkedMembershipIds = ''

    const patreon = await got('https://www.patreon.com/api/oauth2/api/current_user', {
      headers: {
        authorization: `Bearer ${req.body.patreon.access_token}`
      }
    }).json()

    try {
      const patreonInfo = getPatreonInfo(patreon)

      if (req.body.discord) {
        const discord = await got('https://discord.com/api/users/@me', {
          headers: {
            authorization: `Bearer ${req.body.discord.access_token}`
          }
        }).json()
        discordId = discord.id
      }

      if (req.body.bungie) {
        const bungie = await got('https://www.bungie.net/Platform/User/GetMembershipsForCurrentUser/', {
          headers: {
            'X-API-Key': process.env.BUNGIE_API_KEY,
            authorization: `Bearer ${req.body.bungie.access_token}`
          }
        }).json()
        const linkedMemberships = await getAccountsForMembershipId(bungie.Response.bungieNetUser.membershipId)
        linkedMembershipIds = JSON.stringify(linkedMemberships.map(x => x.membershipId))
      }
      
      await createSupporter(req.database, patreonInfo.name, patreonInfo.email, patreonInfo.tier, patreonInfo.id, discordId, linkedMembershipIds)
      return res.end()
    } catch (patreonError) {
      console.log(patreonError)
      console.log(patreon.data, patreon.included)
    }

    return res.status(400).end()
  } catch (error) {
    console.log(error.response && error.response.body ? error.response.body : error)
    return res.status(400).end()
  }
})

const updateSupporters = async (database) => {
  try {
    const jwt = await got('https://www.patreon.com/api/oauth2/token', {
      method: 'POST',
      searchParams: {
        refresh_token: getEnvValue('PATREON_CLIENT_TOKEN'),
        grant_type: 'refresh_token',
        client_id: process.env.PATREON_CLIENT_ID,
        client_secret: process.env.PATREON_CLIENT_SECRET
      }
    }).json()
    setEnvValue('PATREON_CLIENT_TOKEN', jwt.refresh_token)
    const patreon = await got(`https://www.patreon.com/api/oauth2/api/campaigns/${process.env.PATREON_CAMPAIGN}/pledges`, {
      headers: {
        authorization: `Bearer ${jwt.access_token}`
      }
    }).json()

    for (const data of patreon.data) {
      const patreonInfo = getPatreonPledgeInfo(data, patreon.included)

      if (patreonInfo) {
        await updateSupporter(database, patreonInfo.name, patreonInfo.email, patreonInfo.tier, patreonInfo.id)
      }
    }
  } catch (error) {
    console.log(error.response && error.response.body ? error.response.body : error)
  }
}

function getPatreonPledgeInfo (data, included) {
  const user = included.find(x => x.type === 'user' && x.id === data.relationships.patron.data.id)

  if (user && !data.attributes.declined_since) {
    const reward = included.find(x => x.type === 'reward' && x.id === data.relationships.reward.data.id)

    if (reward) {
      const formatter = new Intl.NumberFormat('en-CA', {
        style: 'currency',
        minimumFractionDigits: 0,
        currency: reward.attributes.patron_currency
      })
      return {
        name: user.attributes.full_name,
        email: user.attributes.email,
        tier: `${reward.attributes.title} (${formatter.format(reward.attributes.amount_cents / 100)})`,
        id: user.id
      }
    }
  }
  
  return null
}

function getPatreonInfo (response) {
  const campaign = response.included.find(x => x.type === 'campaign' && x.id === process.env.PATREON_CAMPAIGN)

  if (campaign) {
    const creatorId = campaign.relationships.creator.data.id
    const pledge = response.included.find(x => x.type === 'pledge' && x.relationships.creator.data.id === creatorId)

    if (pledge && !pledge.attributes.declined_since) {
      const reward = response.included.find(x => x.type === 'reward' && x.id === pledge.relationships.reward.data.id)

      if (reward) {
        const formatter = new Intl.NumberFormat('en-CA', {
          style: 'currency',
          minimumFractionDigits: 0,
          currency: reward.attributes.patron_currency
        })
        return {
          name: response.data.attributes.full_name,
          email: response.data.attributes.email,
          tier: `${reward.attributes.title} (${formatter.format(reward.attributes.amount_cents / 100)})`,
          id: response.data.id
        }
      }

      throw new Error('No valid tier reward found')
    }

    throw new Error('No valid pledge found or last payment declined')
  }

  throw new Error('No valid campaign found')
}

module.exports = {
  router,
  updateSupporters
}

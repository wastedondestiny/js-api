const express = require('express')
const { saveLeaderboardEntry } = require('../database/requests')
const getMembershipsById = require('../repositories/user/getMembershipsById')
const { getDestiny1Characters, getDestiny2Characters } = require('../utils/characters')
const { getDestinyAccountModels } = require('../utils/factories')
const { logger } = require('../utils/logger')
const oapi = require('../admin/oapi')
const characterDef = require('../admin/paths/character')

const router = express.Router()

router.get('/:gameVersion/:membershipType/:membershipId', oapi.path(characterDef), async (req, res) => {
  try {
    let characters = []

    switch (+req.params.gameVersion) {
      case 1:
        characters = await getDestiny1Characters(req.params.membershipType, req.params.membershipId)
        break
      case 2:
        characters = await getDestiny2Characters(req.params.membershipType, req.params.membershipId, req.query.language || 'en')
        break
      default:
        throw new Error('Invalid game version')
    }
    
    const memberships = getDestinyAccountModels(await getMembershipsById(req.params.membershipId))
    const membership = memberships.find(x => x.membershipId === req.params.membershipId)

    try {
      await saveLeaderboardEntry(
        req.database,
        +req.params.gameVersion,
        +req.params.membershipType,
        req.params.membershipId,
        membership.bungieDisplayName ?? membership.displayName,
        characters.reduce((a, x) => a + x.timePlayed, 0),
        characters.reduce((a, x) => a + x.deleted ? x.timePlayed : 0, 0),
        characters?.[0]?.triumph ?? 0,
        characters?.[0]?.seals ?? 0
      )
    } catch (err) {
      logger.notify(err)
    }

    const sortedCharacters = characters
      .sort((a, b) => b.timePlayed - a.timePlayed)
      .sort((a, b) => a.deleted - b.deleted)
    return res.json(sortedCharacters)
  } catch (err) {
    console.error(err)
    return res.status(404).json({
      message: `No characters found.`
    })
  }
})

module.exports = router

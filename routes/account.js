const express = require('express')
const { getAccountsForMembershipId } = require('../utils/accounts')
const oapi = require('../admin/oapi')
const accountDef = require('../admin/paths/account')

const router = express.Router()

router.get('/:membershipId', oapi.path(accountDef), async (req, res) => {
  try {
    const destinyAccounts = await getAccountsForMembershipId(req.params.membershipId, req.query.language || 'en')
    return res.json(destinyAccounts)
  } catch (err) {
    console.error(err)
    return res.status(404).json({
      message: `Player ${req.params.membershipId} was not found.`
    })
  }
})

module.exports = router

const express = require('express')
const { getDialogs } = require('../database/requests')
const { getDialogModels } = require('../utils/factories')

const router = express.Router()

router.get('/', async (req, res) => {
  try {
    const dialogs = await getDialogs(req.database)
    return res.json(getDialogModels(dialogs))
  } catch (err) {
    console.error(err)
    return res.json([])
  }
})

module.exports = router

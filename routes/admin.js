const express = require('express')
const AdminJs = require('adminjs')
const AdminJsExpress = require('@adminjs/express')
const AdminJsSequelize = require('@adminjs/sequelize')
const { sequelize, UserModel, LeaderboardModel, PercentileModel } = require('../database')
const { populatePercentiles } = require('../database/requests')
const bcrypt = require('bcrypt')

module.exports = () => {
  if (process.env.ENABLE_ADMIN.toLowerCase() !== 'false') {
    AdminJs.registerAdapter(AdminJsSequelize)
  
    const instance = new AdminJs({
      branding: {
        companyName: 'Wasted on Destiny',
        logo: false
      },
      dashboard: {
        component: AdminJs.bundle('../admin/dashboard.jsx'),
        handler: async () => {
          return {
            d2: await LeaderboardModel.count({ where: { gameVersion: 2 } }),
            d1ps: await LeaderboardModel.count({ where: { gameVersion: 1, membershipType: 2 } }),
            d1xb: await LeaderboardModel.count({ where: { gameVersion: 1, membershipType: 1 } }),
          }
        }
      },
      databases: [sequelize],
      resources: [{
        resource: UserModel,
        options: {
          properties: {
            password: {
              isVisible: false
            },
            setPassword: {
              type: 'string',
              isVisible: { list: false, edit: true, filter: false, show: false }
            }
          },
          actions: {
            new: {
              before: async request => {
                if (request.payload.setPassword) {
                  request.payload = {
                    ...request.payload,
                    password: await bcrypt.hash(request.payload.setPassword, 10),
                    setPassword: undefined
                  }
                }
  
                return request
              }
            }
          }
        }
      }, {
        resource: PercentileModel,
        options: {
          actions: {
            populate: {
              actionType: 'resource',
              icon: 'Star',
              handler: async () => {
                await Promise.all([
                  populatePercentiles(sequelize, 1, 1),
                  populatePercentiles(sequelize, 1, 2),
                  populatePercentiles(sequelize, 2, 1),
                  populatePercentiles(sequelize, 2, 2),
                  populatePercentiles(sequelize, 2, 3),
                  populatePercentiles(sequelize, 2, 5),
                  populatePercentiles(sequelize, 2, -1)
                ])
              },
              component: false
            }
          }
        }
      }],
      rootPath: '/admin'
    })
  
    const router = process.env.REQUIRE_ADMIN_AUTH.toLowerCase() === 'false'
      ? AdminJsExpress.buildRouter(instance)
      : AdminJsExpress.buildAuthenticatedRouter(instance, {
        authenticate: async (email, password) => {
          const user = await UserModel.findOne({ where: { email } })
  
          if (user) {
            const matched = await bcrypt.compare(password, user.password)
  
            if (matched) {
              return user
            }
          }
  
          return false
        },
        cookiePassword: '458w7vhnv543nt768mc247h5xn863475c8o243g'
      })
    return router
  } else {
    const router = express.Router()

    router.post('/', async (req, res) => {
      await Promise.all([
        populatePercentiles(sequelize, 1, 1),
        populatePercentiles(sequelize, 1, 2),
        populatePercentiles(sequelize, 2, 1),
        populatePercentiles(sequelize, 2, 2),
        populatePercentiles(sequelize, 2, 3),
        populatePercentiles(sequelize, 2, 5),
        populatePercentiles(sequelize, 2, -1)
      ])
      res.send('')
    })

    return router
  }
}

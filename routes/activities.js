const express = require('express')
const getProfile = require('../repositories/destiny2/getProfile')
const { getActivityHistory } = require('../utils/activities')
const { getCharacterIds, getLastThirtyDaysActivitiesModel, getActivityBreakdownModel } = require('../utils/factories')
const oapi = require('../admin/oapi')
const activitiesDef = require('../admin/paths/activities')

const router = express.Router()

router.get('/:membershipType/:membershipId', oapi.path(activitiesDef), async (req, res) => {
  try {
    const profile = await getProfile(req.params.membershipType, req.params.membershipId, 0)
    const characters = getCharacterIds(profile)
    const today = new Date()
    const thirtyDaysAgo = new Date(today)
    thirtyDaysAgo.setDate(thirtyDaysAgo.getDate() - 30)

    const allActivities = await Promise.all(characters.map(x => getActivityHistory(req.params.membershipType, req.params.membershipId, x)))
    const flatActivities = allActivities.flat()
    const lastThirtyDays = getLastThirtyDaysActivitiesModel(flatActivities.filter(x => x.period >= thirtyDaysAgo), today, thirtyDaysAgo)
    const breakdown = getActivityBreakdownModel(flatActivities)
    return res.json({
      lastThirtyDays,
      breakdown
    })
  } catch (err) {
    console.error(err)
    return res.status(404).json({
      message: `No activities found.`
    })
  }
})

module.exports = router

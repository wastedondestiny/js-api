const express = require('express')
const getProfile = require('../repositories/destiny2/getProfile')
const { getDestiny2SeasonBreakdown } = require('../utils/factories')
const seasonsList = require('../manifest/seasons.json')
const oapi = require('../admin/oapi')
const seasonDef = require('../admin/paths/season')

const router = express.Router()

router.get('/:membershipType/:membershipId', oapi.path(seasonDef), async (req, res) => {
  try {
    const profile = await getProfile(req.params.membershipType, req.params.membershipId, 3)
    const firstCharacter = profile.characterProgressions.data && Object.values(profile.characterProgressions.data)[0]
    const breakdown = getDestiny2SeasonBreakdown(firstCharacter?.progressions, seasonsList, req.query.language || 'en')
    return res.json(breakdown)
  } catch (err) {
    console.error(err)
    return res.status(404).json({
      message: `No season data found.`
    })
  }
})

module.exports = router

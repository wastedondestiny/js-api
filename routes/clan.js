const express = require('express')
const getGroup = require('../repositories/group/getGroup')
const getGroupByName = require('../repositories/group/getGroupByName')
const getMembersOfGroup = require('../repositories/group/getMembersOfGroup')
const oapi = require('../admin/oapi')
const searchDef = require('../admin/paths/search')
const { getClanModel } = require('../utils/factories')

const router = express.Router()

router.get('/:name', oapi.path(searchDef), async (req, res) => {
  try {
    let group = null

    if (!isNaN(parseInt(req.params.name, 10))) {
      group = await getGroup(req.params.name)
    }

    if (!group) {
      group = await getGroupByName(req.params.name)
    }

    if (!group) {
      return res.status(404).json({
        message: `Clan ${req.params.name} was not found.`
      })
    }

    const members = await getMembersOfGroup(group.detail.groupId)
    const accounts = members.results.map(account => ({
      membershipType: account.destinyUserInfo.membershipType,
      membershipId: account.destinyUserInfo.membershipId
    }))
    return res.json(getClanModel(group, accounts))
  } catch (err) {
    console.error(err)
    return res.status(404).json({
      message: `Clan ${req.params.name} was not found.`
    })
  }
})

module.exports = router

const express = require('express')
const { getLeaderboardPage, getPercentileRank, getSupporters } = require('../database/requests')
const oapi = require('../admin/oapi')
const rankDef = require('../admin/paths/rank')

const router = express.Router()

router.get('/:gameVersion/:membershipType/:membershipId', oapi.path(rankDef), async (req, res) => {
  try {
    const topTen = await getLeaderboardPage(req.database, req.params.gameVersion, req.params.membershipType, 1, 'timePlayed', [])
    const topTenIndex = topTen.findIndex(x => x.membershipId === req.params.membershipId)
    const supporter = await getSupporters(req.database, req.params.membershipId)
    const supportingTier = (supporter?.[0]?.tier && !supporter[0].tier.startsWith('Classic'))
      ? supporter[0].tier.substring(0, supporter[0].tier.indexOf('(') - 1)
      : null

    if (~topTenIndex) {
      return res.json({
        value: topTenIndex + 1,
        isTop: true,
        tier: supportingTier
      })
    } else {
      const rank = await getPercentileRank(req.database, req.params.gameVersion, req.params.membershipType, req.params.membershipId)

      if (!rank) {
        throw new Error('Invalid rank.')
      }

      return res.json({
        value: rank,
        isTop: false,
        tier: supportingTier
      })
    }
  } catch (err) {
    console.error(err)
    return res.status(404).json({
      message: `Time not ranked.`
    })
  }
})

module.exports = router

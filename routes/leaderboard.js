const express = require('express')
const { getLeaderboardPage, getLeaderboardCount, getPrediction, setPrediction, populatePercentiles, populateCount } = require('../database/requests')
const oapi = require('../admin/oapi')
const leaderboardDef = require('../admin/paths/leaderboard')
const getMembersOfGroup = require('../repositories/group/getMembersOfGroup')

const router = express.Router()

router.get('/:gameVersion/:membershipType/:page', oapi.path(leaderboardDef), async (req, res) => {
  try {
    let membershipIds = []

    if (+req.query.clan) {
      const members = await getMembersOfGroup(req.query.clan)
      const accounts = members.results.map(account => account.destinyUserInfo.membershipId)
      membershipIds = accounts
    }

    const sorting = req.query.sorting && ['timePlayed', 'timeWasted', 'triumph', 'seals'].includes(req.query.sorting) ? req.query.sorting : 'timePlayed'
    const [players, platformCount, count] = await Promise.all([
      getLeaderboardPage(req.database, req.params.gameVersion, req.params.membershipType, req.params.page, sorting, membershipIds),
      getLeaderboardCount(req.database, req.params.gameVersion, req.params.membershipType, membershipIds),
      getLeaderboardCount(req.database, req.params.gameVersion, 0, membershipIds)
    ])
    return res.json({
      players,
      page: +req.params.page,
      count: players.length,
      totalPlayers: count,
      totalPlayersPlatform: platformCount
    })
  } catch (err) {
    console.error(err)
    return res.json({
      players: [],
      page: +req.params.page,
      count: 0,
      totalPlayers: 0,
      totalPlayersPlatform: 0
    })
  }
})

router.get('/:gameVersion/:page', oapi.path(leaderboardDef), async (req, res) => {
  try {
    let membershipIds = []

    if (req.query.clan) {
      const members = await getMembersOfGroup(req.query.clan)
      const accounts = members.results.map(account => account.destinyUserInfo.membershipId)
      membershipIds = accounts
    }

    const sorting = req.query.sorting && ['timePlayed', 'timeWasted', 'triumph', 'seals'].includes(req.query.sorting) ? req.query.sorting : 'timePlayed'
    const [players, platformCount, count] = await Promise.all([
      getLeaderboardPage(req.database, req.params.gameVersion, -1, req.params.page, sorting, membershipIds),
      getLeaderboardCount(req.database, req.params.gameVersion, 0, membershipIds),
      getLeaderboardCount(req.database, req.params.gameVersion, 0, membershipIds)
    ])
    return res.json({
      players,
      page: +req.params.page,
      count: players.length,
      totalPlayers: count,
      totalPlayersPlatform: platformCount
    })
  } catch (err) {
    console.error(err)
    return res.json({
      players: [],
      page: +req.params.page,
      count: 0,
      totalPlayers: 0,
      totalPlayersPlatform: 0
    })
  }
})

router.get('/prediction', async (req, res) => {
  if (!req.headers['season'] || !req.headers['membership']) {
    return res.json({
      result: ''
    })
  }

  const prediction = await getPrediction(req.database, req.headers['season'], req.headers['membership'])
  return res.json({
    result: prediction?.predictions ?? ''
  })
})

router.put('/prediction', async (req, res) => {
  if (!req.headers['season'] || !req.headers['membership']) {
    return res.status(400).send('Bad request')
  }

  const prediction = await getPrediction(req.database, req.headers['season'], req.headers['membership'])

  if (!prediction) {
    await setPrediction(req.database, req.headers['season'], req.headers['membership'], Object.keys(req.body)?.[0])
  }

  return res.send('Ok')
})

function materializeLeaderboard (database) {
  return Promise.all([
    populatePercentiles(database, 1, 1),
    populatePercentiles(database, 1, 2),
    populatePercentiles(database, 2, 1),
    populatePercentiles(database, 2, 2),
    populatePercentiles(database, 2, 3),
    populatePercentiles(database, 2, 5),
    populatePercentiles(database, 2, 6),
    populatePercentiles(database, 2, -1),
    populateCount(database)
  ])
}

module.exports = {
  router,
  materializeLeaderboard
}

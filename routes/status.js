const express = require('express')
const { getGlobalAlertModels } = require('../utils/factories')
const oapi = require('../admin/oapi')
const statusDef = require('../admin/paths/status')
const getGlobalAlerts = require('../repositories/core/getGlobalAlerts')
const getCommonSettings = require('../repositories/core/getCommonSettings')

const router = express.Router()

router.get('/', oapi.path(statusDef), async (req, res) => {
  try {
    const alerts = await getGlobalAlerts(req.query.language || 'en')
    const settings = await getCommonSettings()
    return res.json(getGlobalAlertModels(alerts, settings))
  } catch (err) {
    console.error(err)
    return res.json([])
  }
})

module.exports = router

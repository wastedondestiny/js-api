const express = require('express')

const router = express.Router()

router.get('/', async (req, res) => {
  res.set('Content-Type', 'text/html')
  res.send(Buffer.from(`
    <!DOCTYPE html>
    <html>
    <head>
      <meta charset="utf-8">
      <script type="module" src="https://unpkg.com/rapidoc/dist/rapidoc-min.js"></script>
    </head>
    <body>
      <rapi-doc
        spec-url="/openapi.json"
        theme="dark"
        schema-style="table"
        show-header="false"
        use-path-in-nav-bar="true"
        render-style="focused"
        allow-authentication="false"
      >
        <img
          slot="nav-logo"
          src="https://wastedondestiny.com/img/icons/android-chrome-256x256.png"
        />
      </rapi-doc>
    </body>
    </html>
  `))
})

module.exports = router

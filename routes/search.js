const express = require('express')
const { isSteam64Format } = require('../utils')
const getMembershipFromHardLinkedCredential = require('../repositories/user/getMembershipFromHardLinkedCredential')
const searchDestinyPlayerByBungieName = require('../repositories/destiny2/searchDestinyPlayerByBungieName')
const searchDestinyPlayer = require('../repositories/destiny/searchDestinyPlayer')
const searchUserByPrefix = require('../repositories/user/searchUserByPrefix')
const searchPlayer = require('../repositories/trials.report/searchPlayer')
const { getAccountsForMembershipId } = require('../utils/accounts')
const oapi = require('../admin/oapi')
const searchDef = require('../admin/paths/search')

const router = express.Router()

router.get('/:displayName', oapi.path(searchDef), async (req, res) => {
  try {
    const foundDestinyAccounts = isSteam64Format(req.params.displayName)
      // If a SteamId64 is found, search the account for it
      ? [await getMembershipFromHardLinkedCredential(req.params.displayName)]
      // If it's a display name, get the first linked account
      : await Promise.all([
          searchDestinyPlayerByBungieName(req.params.displayName),
          searchUserByPrefix(req.params.displayName),
          searchDestinyPlayer(req.params.displayName),
          searchPlayer(req.params.displayName)
        ])
    const firstDestinyAccount = foundDestinyAccounts.flat()[0];

    // Using the first account found, find all linked memberships
    const destinyAccounts = await getAccountsForMembershipId(firstDestinyAccount?.membershipId, req.query.language || 'en')
    return res.json(destinyAccounts)
  } catch (err) {
    console.error(err)
    return res.status(404).json({
      message: `Player ${req.params.displayName} was not found.`
    })
  }
})

router.get('/:membershipType/:displayName', oapi.path(searchDef), async (req, res) => {
  try {
    const foundDestinyAccounts = await Promise.all([
      searchDestinyPlayerByBungieName(req.params.displayName, req.params.membershipType),
      searchUserByPrefix(req.params.displayName, req.params.membershipType),
      searchDestinyPlayer(req.params.displayName, req.params.membershipType),
      searchPlayer(req.params.displayName, req.params.membershipType)
    ])
    const firstDestinyAccount = foundDestinyAccounts.flat()[0];

    // Using the first account found, find all linked memberships
    const destinyAccounts = await getAccountsForMembershipId(firstDestinyAccount?.membershipId, req.query.language || 'en')
    return res.json(destinyAccounts)
  } catch (err) {
    console.error(err)
    return res.status(404).json({
      message: `Player ${req.params.displayName} was not found.`
    })
  }
})

module.exports = router

const { destiny2 } = require('../bungie')

module.exports = async (groupName) => {
  if (!groupName) {
    return null
  }

  try {
    return await destiny2(`Platform/GroupV2/NameV2/`, {}, {
      groupName: groupName,
      groupType: 1 // General = 0, Clan = 1
    }, 'POST')
  } catch (_) {}

  return null
}

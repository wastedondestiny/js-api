const { destiny2 } = require('../bungie')

// GroupsForMemberFilter 0 = All
// GroupType 1 = Clan
module.exports = (membershipType, membershipId) => destiny2(`Platform/GroupV2/User/${membershipType}/${membershipId}/0/1`)

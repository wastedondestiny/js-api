const { destiny2 } = require('../bungie')

module.exports = groupId => destiny2(`Platform/GroupV2/${groupId}`)

const { destiny2 } = require('../bungie')

module.exports = async (displayNamePrefix, membershipType = 0) => {
  try {
    const results = await destiny2(`Platform/User/Search/Prefix/${encodeURIComponent(displayNamePrefix)}/${membershipType}/`)
    
    if (results.searchResults.length) {
      return results.searchResults[0].destinyMemberships
    }
  } catch (_) {}

  return []
}

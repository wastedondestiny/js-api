const { destiny2 } = require('../bungie')

module.exports = hardLinkedCredential => destiny2(`Platform/User/GetMembershipFromHardLinkedCredential/12/${hardLinkedCredential}/`)

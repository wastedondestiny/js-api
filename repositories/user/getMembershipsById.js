const { destiny2 } = require('../bungie')

module.exports = async membershipId => {
  const results = await destiny2(`Platform/User/GetMembershipsById/${membershipId}/-1/`)
  return results.destinyMemberships
}

const { destiny2 } = require('../bungie')

module.exports = (locale = 'en') => destiny2(`Platform/GlobalAlerts/?lc=${locale}`)

const { destiny } = require('../bungie')

module.exports = (membershipType, membershipId) => destiny(`Platform/Destiny/${membershipType}/Account/${membershipId}`)

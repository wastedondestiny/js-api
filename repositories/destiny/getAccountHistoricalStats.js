const { destiny } = require('../bungie')

module.exports = (membershipType, membershipId) => destiny(`Platform/Destiny/Stats/Account/${membershipType}/${membershipId}`)

const { destiny } = require('../bungie')

module.exports = async (displayName, membershipType = -1) => {
  try {
    return await destiny(`Platform/Destiny/SearchDestinyPlayer/${membershipType}/${encodeURIComponent(displayName)}`)
  } catch (_) {}

  return []
}

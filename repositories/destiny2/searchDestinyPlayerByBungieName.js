const { destiny2 } = require('../bungie')

module.exports = async (bungieName, membershipType = 'All') => {
  const [displayName, displayNameCode] = bungieName.split('#')

  if (!displayName || !displayNameCode) {
    return []
  }

  try {
    return await destiny2(`Platform/Destiny2/SearchDestinyPlayerByBungieName/${membershipType}/`, {}, {
      displayName: encodeURIComponent(displayName),
      displayNameCode
    }, 'POST')
  } catch (_) {}

  return []
}

const { destiny2 } = require('../bungie')

module.exports = (membershipType, membershipId) => destiny2(`Platform/Destiny2/${membershipType}/Account/${membershipId}/Stats`)

const { destiny2 } = require('../bungie')

module.exports = async membershipId => {
  const results = await destiny2(`Platform/Destiny2/-1/Profile/${membershipId}/LinkedProfiles/`)
  return results.profilesWithErrors
}

const { destiny2 } = require('../bungie')

module.exports = (membershipType, membershipId, characterId, page) => destiny2(
  `Platform/Destiny2/${membershipType}/Account/${membershipId}/Character/${characterId}/Stats/Activities`,
  {
    mode: 'None',
    count: 250,
    page
  }
)

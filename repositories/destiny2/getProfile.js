const { destiny2 } = require('../bungie')

const details = [
  [
    200  // Characters
  ],
  [
    100, // Profiles
    204, // CharacterActivities
    900  // Records
  ],
  [
    100, // Profiles
    104, // ProfileProgression
    200, // Characters
    202, // CharacterProgressions
    204, // CharacterActivities
    900  // Records
  ],
  [
    202  // ProfileProgression
  ]
]

module.exports = (membershipType, membershipId, detailsLevel = 2) => destiny2(`Platform/Destiny2/${membershipType}/Profile/${membershipId}/`, {
  components: (details[detailsLevel] ?? details[2]).join(',')
})

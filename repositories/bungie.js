const { got } = require('got-cjs')

const call = prefixUrl => async (url, searchParams = undefined, body = undefined, method = 'GET') => {
  let response

  try {
    response = await got(url, {
      prefixUrl,
      method,
      searchParams,
      json: body,
      headers: {
        'X-API-Key': process.env.BUNGIE_API_KEY
      }
    }).json()
  } catch (err) {
    throw new Error(`Invalid response from url (${url})`, err)
  }
  
  if (response.Response) {
    return response.Response
  }

  throw new Error(`Cannot parse response from url (${url}): ${response}`)
}

module.exports = {
  destiny2: call('https://www.bungie.net'),
  destiny: call('https://www.bungie.net/d1')
}

const { got } = require('got-cjs')

module.exports = async (displayName, membershipType = 0) => {
  try {
    const result = await got(`https://elastic.destinytrialsreport.com/players/${membershipType}/${encodeURIComponent(displayName)}/`).json()
    return result
  } catch (_) {}

  return []
}

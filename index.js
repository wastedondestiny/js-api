const dotenv = require('dotenv')
dotenv.config()

const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const helmet = require('helmet')
const cors = require('cors')
const responseTime = require('response-time')
const cron = require('node-cron')
const { sequelize, sync } = require('./database')
const cache = require('./middlewares/cache')
const version = require('./middlewares/version')
const database = require('./middlewares/database')
const clanRoutes = require('./routes/clan')
const statusRoutes = require('./routes/status')
const searchRoutes = require('./routes/search')
const seasonRoutes = require('./routes/season')
const accountRoutes = require('./routes/account')
const characterRoutes = require('./routes/character')
const activitiesRoutes = require('./routes/activities')
const { router: leaderboardRoutes, materializeLeaderboard } = require('./routes/leaderboard')
const { router: supportersRoutes, updateSupporters } = require('./routes/supporters')
const dialogsRoutes = require('./routes/dialogs')
const rankRoutes = require('./routes/rank')
const adminRoutes = require('./routes/admin')
const docsRoutes = require('./routes/docs')
const oapi = require('./admin/oapi')
const { plugin: loggerPlugin, logger } = require('./utils/logger')

const app = express()
const port = process.env.PORT || 3000

const middlewares = [
  bodyParser.urlencoded({ extended: false }),
  express.json(),
  responseTime(),
  helmet(),
  cors(),
  version,
  database(sequelize),
  oapi
]

app.use(loggerPlugin.requestHandler)
app.use(morgan('tiny'))
app.use(oapi)

app.use('/clan', middlewares, cache(3600), clanRoutes)
app.use('/status', middlewares, cache(300), statusRoutes)
app.use('/search', middlewares, cache(3600), searchRoutes)
app.use('/season', middlewares, cache(1800), seasonRoutes)
app.use('/account', middlewares, cache(43200), accountRoutes)
app.use('/character', middlewares, cache(43200), characterRoutes)
app.use('/leaderboard', middlewares, cache(43200), leaderboardRoutes)
app.use('/activities', middlewares, cache(1800), activitiesRoutes)
app.use('/supporters', middlewares, cache(3600), supportersRoutes)
app.use('/dialogs', middlewares, cache(3600), dialogsRoutes)
app.use('/rank', middlewares, cache(43200), rankRoutes)
app.use('/docs', docsRoutes)
app.use('/admin', adminRoutes())

app.get('*', (req, res) => {
  logger.notify('Not Found', req)
  res.status(404).send()
})

app.use(loggerPlugin.errorHandler)

cron.schedule('0 * * * *', async () => {
  // await sync()
  await Promise.all([
    updateSupporters(sequelize),
    materializeLeaderboard(sequelize)
  ])
  console.log('Finished applying migrations!')
})

sync()
  .then(async () => {
    if (process.env.SKIP_SYNC_CRON !== 'true' && process.env.NODE_ENV !== 'production') {
      await Promise.all([
        updateSupporters(sequelize),
        materializeLeaderboard(sequelize)
      ])
      console.log('Finished applying migrations!')
    } else {
      console.log('Skipping migrations, will execute on the next hour')
    }
  })
  .catch(console.log)
  .finally(() => {
    app.listen(port, () => {
      console.log(`Application restarted on port ${port}`)
      console.log(`Wasted on Destiny API - Listening on port ${port}`)
    })
  })

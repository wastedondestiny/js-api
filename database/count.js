const { DataTypes } = require('sequelize')

module.exports = sequelize => sequelize.define('counts', {
  membershipType: { type: DataTypes.TINYINT, required: true, primaryKey: true },
  gameVersion: { type: DataTypes.TINYINT, required: true, primaryKey: true },
  count: { type: DataTypes.INTEGER, required: true }
}, {
  sequelize,
  tableName: 'counts',
  timestamps: false,
  charset: 'utf8mb4',
  collate: 'utf8mb4_bin'
})

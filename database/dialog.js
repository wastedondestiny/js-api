const { DataTypes } = require('sequelize')

module.exports = sequelize => sequelize.define('dialogs', {
  title: { type: DataTypes.STRING, required: true, allowNull: false },
  text: { type: DataTypes.TEXT, required: true, allowNull: false },
  image: { type: DataTypes.BLOB, allowNull: true },
  expiry: { type: DataTypes.DATE, allowNull: true }
}, {
  sequelize,
  tableName: 'dialogs',
  charset: 'utf8mb4',
  collate: 'utf8mb4_bin'
})

const { DataTypes, literal } = require('sequelize')

module.exports = sequelize => sequelize.define('leaderboard', {
  membershipId: { type: DataTypes.STRING(50), required: true, primaryKey: true },
  membershipType: { type: DataTypes.TINYINT, required: true, primaryKey: true },
  gameVersion: { type: DataTypes.TINYINT, required: true, primaryKey: true },
  timePlayed: { type: DataTypes.INTEGER, allowNull: false, required: true },
  timeWasted: { type: DataTypes.INTEGER, allowNull: true },
  triumph: { type: DataTypes.INTEGER, allowNull: true },
  seals: { type: DataTypes.INTEGER, allowNull: true },
  displayName: { type: DataTypes.STRING(50), allowNull: false, required: true },
  updateTime: { type: DataTypes.DATE, defaultValue: literal('CURRENT_TIMESTAMP'), allowNull: false, required: true }
}, {
  sequelize,
  tableName: 'leaderboard',
  timestamps: false,
  charset: 'utf8mb4',
  collate: 'utf8mb4_bin',
  indexes: [
    { fields: ['timePlayed'] },
    { fields: ['timeWasted'] },
    { fields: ['triumph'] },
    { fields: ['seals'] }
  ]
})

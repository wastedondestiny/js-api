const { DataTypes } = require('sequelize')

module.exports = sequelize => sequelize.define('users', {
  email: { type: DataTypes.STRING, required: true },
  password: { type: DataTypes.STRING, required: true }
}, {
  sequelize,
  tableName: 'supporters',
  charset: 'utf8mb4',
  collate: 'utf8mb4_bin'
})

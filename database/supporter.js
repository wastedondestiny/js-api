const { DataTypes } = require('sequelize')

module.exports = sequelize => sequelize.define('supporters', {
  id: { type: DataTypes.STRING(50), primaryKey: true },
  name: { type: DataTypes.STRING, allowNull: false },
  email: { type: DataTypes.STRING, allowNull: false },
  tier: { type: DataTypes.STRING, allowNull: true },
  linkedMembershipIds: { type: DataTypes.JSON, allowNull: true },
  discord: { type: DataTypes.STRING(50), allowNull: true }
}, {
  sequelize,
  tableName: 'supporters',
  charset: 'utf8mb4',
  collate: 'utf8mb4_bin'
})

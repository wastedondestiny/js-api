const { Sequelize } = require('sequelize')
const User = require('./user')
const Leaderboard = require('./leaderboard')
const Percentile = require('./percentile')
const Count = require('./count')
const Dialog = require('./dialog')
const Supporter = require('./supporter')
const Prediction = require('./prediction')

const sequelize = new Sequelize(process.env.MYSQL_DATABASE, process.env.MYSQL_USER, process.env.MYSQL_PASSWORD, {
  dialect: 'mysql',
  host: process.env.MYSQL_HOST,
  port: process.env.MYSQL_PORT || 3306,
  pool: {
    max: 20,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
})

const UserModel = User(sequelize)
const LeaderboardModel = Leaderboard(sequelize)
const PercentileModel = Percentile(sequelize)
const CountModel = Count(sequelize)
const DialogModel = Dialog(sequelize)
const SupporterModel = Supporter(sequelize)
const PredictionModel = Prediction(sequelize)

module.exports = {
  sequelize,
  sync: () => sequelize.sync({ alter: true }),
  UserModel,
  LeaderboardModel,
  PercentileModel,
  CountModel,
  DialogModel,
  SupporterModel,
  PredictionModel
}

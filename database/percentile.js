const { DataTypes } = require('sequelize')

module.exports = sequelize => sequelize.define('percentiles', {
  membershipType: { type: DataTypes.TINYINT, required: true, primaryKey: true },
  gameVersion: { type: DataTypes.TINYINT, required: true, primaryKey: true },
  timePlayed: { type: DataTypes.INTEGER, required: true },
  rank: { type: DataTypes.TINYINT, required: true, primaryKey: true }
}, {
  sequelize,
  tableName: 'percentiles',
  timestamps: false,
  charset: 'utf8mb4',
  collate: 'utf8mb4_bin'
})

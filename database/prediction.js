const { DataTypes } = require('sequelize')

module.exports = sequelize => sequelize.define('predictions', {
  membershipId: { type: DataTypes.STRING },
  season: { type: DataTypes.INTEGER },
  predictions: { type: DataTypes.STRING }
}, {
  sequelize,
  tableName: 'predictions',
  charset: 'utf8mb4',
  collate: 'utf8mb4_bin'
})

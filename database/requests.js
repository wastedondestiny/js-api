const { PercentileModel, PredictionModel } = require('./')
const { QueryTypes } = require('sequelize')

module.exports = {
  getDialogs: async database => {
    const results = await database.query(
      'SELECT * FROM dialogs WHERE expiry>NOW();',
      { type: QueryTypes.SELECT }
    )
    return results
  },
  getLeaderboardPage: async (database, gameVersion, membershipType, page, column, membershipIds) => {
    let filter = ''

    if (membershipIds.length) {
      filter = 'AND membershipId IN (:membershipIds)'
    }

    if (membershipType > 0) {
      const results = await database.query(
        `SELECT membershipId, membershipType, gameVersion, displayName, ${column} AS value
         FROM leaderboard WHERE membershipType=:membershipType AND gameVersion=:gameVersion
         ${filter}
         ORDER BY ${column} DESC LIMIT :limit OFFSET :offset;`,
        { type: QueryTypes.SELECT, replacements: { membershipType, gameVersion, limit: 10, offset: (page - 1) * 10, membershipIds } }
      )
      return results
    } else {
      const results = await database.query(
        `SELECT membershipId, membershipType, gameVersion, displayName, ${column} AS value
         FROM leaderboard WHERE gameVersion=:gameVersion
         ${filter}
         ORDER BY ${column} DESC LIMIT :limit OFFSET :offset;`,
        { type: QueryTypes.SELECT, replacements: { gameVersion, limit: 10, offset: (page - 1) * 10, membershipIds } }
      )
      return results
    }
  },
  getLeaderboardCount: async (database, gameVersion, membershipType, membershipIds) => {
    if (membershipIds.length) {
      if (membershipType > 0) {
        const results = await database.query(
          'SELECT COUNT(*) AS `count` FROM leaderboard WHERE gameVersion=:gameVersion AND membershipType=:membershipType AND membershipId IN (:membershipIds);',
          { type: QueryTypes.SELECT, plain: true, replacements: { gameVersion, membershipType, membershipIds } }
        )
        return results?.count ?? 0
      } else {
        const results = await database.query(
          'SELECT COUNT(*) AS `count` FROM leaderboard WHERE gameVersion=:gameVersion AND membershipId IN (:membershipIds);',
          { type: QueryTypes.SELECT, plain: true, replacements: { gameVersion, membershipIds } }
        )
        return results?.count ?? 0
      }
    }

    if (membershipType > 0) {
      const results = await database.query(
        'SELECT `count` FROM counts WHERE gameVersion=:gameVersion AND membershipType=:membershipType;',
        { type: QueryTypes.SELECT, plain: true, replacements: { gameVersion, membershipType } }
      )
      return results?.count ?? 0
    } else {
      const results = await database.query(
        'SELECT `count` FROM counts WHERE gameVersion=:gameVersion AND membershipType=-1;',
        { type: QueryTypes.SELECT, plain: true, replacements: { gameVersion } }
      )
      return results?.count ?? 0
    }
  },
  saveLeaderboardEntry: (database, gameVersion, membershipType, membershipId, displayName, timePlayed, timeWasted, triumph, seals) => {
    return database.query(
      `INSERT INTO leaderboard (membershipId, membershipType, gameVersion, displayName, timePlayed, timeWasted, triumph, seals)
      VALUES (:membershipId, :membershipType, :gameVersion, :displayName, :timePlayed, :timeWasted, :triumph, :seals)
      ON DUPLICATE KEY UPDATE displayName=:displayName, timePlayed=:timePlayed, timeWasted=:timeWasted, triumph=:triumph, seals=:seals, updateTime=NOW();`,
      { type: QueryTypes.INSERT, replacements: { membershipId, membershipType, gameVersion, displayName, timePlayed, timeWasted, triumph, seals } }
    )
  },
  getPercentileRank: async (database, gameVersion, membershipType, membershipId) => {
    if (membershipType > 0) {
      const results = await database.query(
        `SELECT r.\`rank\` FROM (SELECT \`rank\`, timePlayed FROM percentiles WHERE membershipType=:membershipType AND gameVersion=:gameVersion ORDER BY \`rank\` ASC) r
        INNER JOIN (SELECT timePlayed FROM leaderboard WHERE membershipType=:membershipType AND gameVersion=:gameVersion AND membershipId=:membershipId) l ON 1=1
        WHERE l.timePlayed > r.timePlayed LIMIT 1;`,
        { type: QueryTypes.SELECT, plain: true, replacements: { membershipType, gameVersion, membershipId } }
      )
      return results?.rank ?? 0
    } else {
      const results = await database.query(
        `SELECT r.\`rank\` FROM (SELECT \`rank\`, timePlayed FROM percentiles WHERE membershipType=-1 AND gameVersion=:gameVersion ORDER BY \`rank\` ASC) r
        INNER JOIN (SELECT timePlayed FROM leaderboard WHERE gameVersion=:gameVersion AND membershipId=:membershipId) l ON 1=1
        WHERE l.timePlayed > r.timePlayed LIMIT 1;`,
        { type: QueryTypes.SELECT, plain: true, replacements: { gameVersion, membershipId } }
      )
      return results?.rank ?? 0
    }
  },
  populatePercentiles: async (database, gameVersion, membershipType) => {
    let results = []
    
    if (membershipType > 0) {
      results = await database.query(
        `SELECT ${membershipType} AS membershipType, ${gameVersion} AS gameVersion, MIN(d.timePlayed) AS timePlayed, d.percentileRank + 1 AS \`rank\`
        FROM (SELECT c.timePlayed, ROUND(((@rank - \`rank\`) / @rank) * 99, 0) AS percentileRank
        FROM (SELECT *, @prev:=@curr, @curr:=a.timePlayed, @rank:=IF(@prev=@curr, @rank, @rank + 1) AS \`rank\`
        FROM (SELECT membershipId, timePlayed FROM leaderboard WHERE membershipType=${membershipType} AND gameVersion=${gameVersion}) AS a,
        (SELECT @curr:=null, @prev:=null, @rank:=0) AS b ORDER BY timePlayed) AS c) AS d GROUP BY d.percentileRank;`,
        { type: QueryTypes.SELECT, model: PercentileModel, mapToModel: true }
      )
    } else {
      results = await database.query(
        `SELECT ${membershipType} AS membershipType, ${gameVersion} AS gameVersion, MIN(d.timePlayed) AS timePlayed, d.percentileRank + 1 AS \`rank\`
        FROM (SELECT c.timePlayed, ROUND(((@rank - \`rank\`) / @rank) * 99, 0) AS percentileRank
        FROM (SELECT *, @prev:=@curr, @curr:=a.timePlayed, @rank:=IF(@prev=@curr, @rank, @rank + 1) AS \`rank\`
        FROM (SELECT membershipId, timePlayed FROM leaderboard WHERE gameVersion=${gameVersion}) AS a,
        (SELECT @curr:=null, @prev:=null, @rank:=0) AS b ORDER BY timePlayed) AS c) AS d GROUP BY d.percentileRank;`,
        { type: QueryTypes.SELECT, model: PercentileModel, mapToModel: true }
      )
    }
    
    if (results.length) {
      await PercentileModel.bulkCreate(results.map(x => x.get()), { updateOnDuplicate: ['timePlayed'] })
    }
  },
  populateCount: async (database) => {
    return await database.query(
      `REPLACE INTO counts (\`count\`, gameVersion, membershipType)
      SELECT COUNT(*) AS \`count\`, 1 AS gameVersion, 1 AS membershipType FROM leaderboard WHERE gameVersion=1 AND membershipType=1 UNION
      SELECT COUNT(*) AS \`count\`, 1 AS gameVersion, 2 AS membershipType FROM leaderboard WHERE gameVersion=1 AND membershipType=2 UNION
      SELECT COUNT(*) AS \`count\`, 1 AS gameVersion, -1 AS membershipType FROM leaderboard WHERE gameVersion=1 UNION
      SELECT COUNT(*) AS \`count\`, 2 AS gameVersion, 1 AS membershipType FROM leaderboard WHERE gameVersion=2 AND membershipType=1 UNION
      SELECT COUNT(*) AS \`count\`, 2 AS gameVersion, 2 AS membershipType FROM leaderboard WHERE gameVersion=2 AND membershipType=2 UNION
      SELECT COUNT(*) AS \`count\`, 2 AS gameVersion, 3 AS membershipType FROM leaderboard WHERE gameVersion=2 AND membershipType=3 UNION
      SELECT COUNT(*) AS \`count\`, 2 AS gameVersion, 5 AS membershipType FROM leaderboard WHERE gameVersion=2 AND membershipType=5 UNION
      SELECT COUNT(*) AS \`count\`, 2 AS gameVersion, 6 AS membershipType FROM leaderboard WHERE gameVersion=2 AND membershipType=6 UNION
      SELECT COUNT(*) AS \`count\`, 2 AS gameVersion, -1 AS membershipType FROM leaderboard WHERE gameVersion=2 UNION
      SELECT COUNT(*) AS \`count\`, -1 AS gameVersion, -1 AS membershipType FROM leaderboard;`
    )
  },
  getSupporters: async (database, membershipId = null) => {
    if (membershipId) {
      const results = await database.query(
        `SELECT * FROM supporters WHERE JSON_CONTAINS(linkedMembershipIds, \'"${membershipId}"\', \'$\');`,
        { type: QueryTypes.SELECT }
      )
      return results
    } else {
      const results = await database.query(
        'SELECT * FROM supporters WHERE tier NOT LIKE \'Classic%\';',
        { type: QueryTypes.SELECT }
      )
      return results
    }
  },
  createSupporter: async (database, name, email, tier, patreonId, discordId, membershipIds) => {
    await database.query(
      `INSERT INTO supporters (id, name, email, tier, linkedMembershipIds, discord, createdAt, updatedAt)
      VALUES (?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP())
      ON DUPLICATE KEY UPDATE name=?, email=?, tier=?, linkedMembershipIds=?, discord=?, updatedAt=CURRENT_TIMESTAMP();`,
      { type: QueryTypes.INSERT, replacements: [
        patreonId, name, email, tier, membershipIds, discordId,
                   name, email, tier, membershipIds, discordId
      ] }
    )
  },
  updateSupporter: async (database, name, email, tier, patreonId) => {
    await database.query(
      `INSERT INTO supporters (id, name, email, tier, createdAt, updatedAt)
      VALUES (?, ?, ?, ?, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP())
      ON DUPLICATE KEY UPDATE name=?, email=?, tier=?, updatedAt=CURRENT_TIMESTAMP();`,
      { type: QueryTypes.INSERT, replacements: [
        patreonId, name, email, tier,
                   name, email, tier
      ] }
    )
  },
  getPrediction: async (database, season, membershipId) => {
    const results = await database.query(
      'SELECT predictions FROM predictions WHERE season = ? AND membershipId = ?',
      { type: QueryTypes.SELECT, model: PredictionModel, mapToModel: true, replacements: [+season, membershipId] }
    )
    return results.length ? results[0] : null
  },
  setPrediction: async (database, season, membershipId, prediction) => {
    await database.query(
      'INSERT INTO predictions (season, membershipId, predictions, createdAt, updatedAt) VALUES (?, ?, ?, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP())',
      { type: QueryTypes.INSERT, replacements: [+season, membershipId, prediction] }
    )
  }
}

Time Wasted on Destiny
====================

## About
Time Wasted on Destiny is a website that tells you how much time you've spent on the Destiny series. On top of this, it is the only website that tells you just how much time you've spent on deleted characters, which you could consider wasted time.

**NOTE**: This page is only a documentation for https://v2.api.wastedondestiny.com. If you're looking for information about the website itself, please go to https://gitlab.com/wastedondestiny/website.

## How to use
The API for Time Wasted on Destiny is really simple and easy to use.

The documentation is available here: https://v2.api.wastedondestiny.com/docs

Keep in mind that this API is released free of charge and without subscription. Please do not abuse, or I will have to restrict access to the API.

## Installing
### Requirements
- Node 8.11+
- NPM 6.4+

Execute `npm install` to install dependencies, then run `npm run start` to start a local development server.
To compile for production, run `npm run build`.
Don't forget to create a `.env` file and supply your own API keys and configuration, like so:

```
BUNGIE_API_KEY=00000000000000000000000000000000000
MYSQL_HOST=localhost
MYSQL_USER=root
MYSQL_PASSWORD=
MYSQL_DATABASE=wastedondestiny
BUGSNAG_API_KEY=00000000000000000000000000000000000
ENABLE_ADMIN=true
REQUIRE_ADMIN_AUTH=true
```

## Disclaimer
Time Wasted on Destiny is not affiliated with Bungie. Destiny is a registered trademark of Bungie.

## Licence
This project is distributed under the GPL v3.0 license. Please give credit if you intend on using it!

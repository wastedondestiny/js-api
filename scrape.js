const dotenv = require('dotenv')
dotenv.config()

const { got } = require('got-cjs')
const fs = require('fs')
const async = require('async')
const chalk = require('chalk')
const BottomBar = require('bottom-bar')
const { QueryTypes } = require('sequelize')
const { sequelize } = require('./database')

const getPlatform = account => {
  let game = account.gameVersion === 2 ? 'D2' : 'D1'

  switch (account.membershipType) {
    case 1: return `Xbox ${game}`
    case 2: return `Playstation ${game}`
    case 3: return `Steam ${game}`
    case 5: return `Stadia ${game}`
    case 6: return `Epic Games ${game}`
  }
}

const scrape = async (bar, membershipId) => {
  try {
    const accounts = await got(`http://localhost:3000/account/${membershipId}`).json()

    for await (const account of accounts) {
      const platform = getPlatform(account)
      let count = 0

      try {
        const characters = await got(`http://localhost:3000/character/${account.gameVersion}/${account.membershipType}/${account.membershipId}`).json()
        count += characters.length
      } catch (_) { }
      
      const accountMessage = chalk.blueBright(`Scraped membershipId ${membershipId}, found ${account.displayName} on ${platform},`)
      const characterMessage = count > 0 ? chalk.greenBright(` and found and saved ${count} characters!`) : chalk.red(' but could not find any character.')
      bar.log(accountMessage + characterMessage)
    }
  } catch (e) {
    bar.log(chalk.red(`Could not scrape account for membershipId ${membershipId}`))
  }
}

const wrapLog = bar => {
  const file = fs.createWriteStream(`./logs/scrape-${new Date().toISOString().replaceAll(':', '-')}.log`)
  const barLog = bar.log

  function newLog () {
    const [message, ...args] = arguments
    barLog.apply(bar, [message, ...args])
    const cleanMessage = message.replace(/[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g, '')
    file.write.apply(file, [cleanMessage + '\n', ...args])
  }

  bar.log = newLog
  return bar
}

sequelize.query('SELECT membershipId FROM leaderboard WHERE updateTime < \'2022-12-17 17:00:00\'', { type: QueryTypes.SELECT }).then(async accounts => {
  console.log(chalk.green(`Got ${accounts.length} accounts! Starting scraping...`))
  const bar = wrapLog(new BottomBar())
  const queue = async.queue(async membershipId => {
    await scrape(bar, membershipId)
    bar.update(accounts.length - queue.length(), accounts.length)
  }, 1000)
  queue.drain(() => {
    bar.destroy()
    process.exit()
  })

  accounts.forEach(account => {
    queue.push(account.membershipId)
  })
})

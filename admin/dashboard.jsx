import React, { useEffect, useState } from 'react'
import { Box, Header, Label } from '@adminjs/design-system'
import { ApiClient } from 'adminjs'

const api = new ApiClient()

const Dashboard = () => {
  const [data, setData] = useState({})

  useEffect(() => {
    api.getDashboard().then(x => setData(x.data))
  }, [])

  const formatNumber = number => number.toLocaleString('en-US')

  return (
    <Box flex variant="grey">
      <Box width={1/3} mr="lg">
        <Box variant="white">
          <Header.H3>Destiny 2 Cross Play Players</Header.H3>
          <Label fontSize="h1">{formatNumber(data?.d2 ?? 0)}</Label>
        </Box>
      </Box>
      <Box width={1/3} ml="lg" mr="lg">
        <Box variant="white">
          <Header.H3>Destiny Playstation Players</Header.H3>
          <Label fontSize="h1">{formatNumber(data?.d1ps ?? 0)}</Label>
        </Box>
      </Box>
      <Box width={1/3} ml="lg">
        <Box variant="white">
          <Header.H3>Destiny Xbox Players</Header.H3>
          <Label fontSize="h1">{formatNumber(data?.d1xb ?? 0)}</Label>
        </Box>
      </Box>
    </Box>
  )
}

export default Dashboard
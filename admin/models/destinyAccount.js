module.exports = {
  type: 'object',
  properties: {
    gameVersion: {
      description: 'The version of the game the account is on (Destiny 1 or Destiny 2)',
      type: 'integer',
      enum: [1, 2]
    },
    displayName: {
      description: 'The name displayed in the game, or the platform name if not found',
      type: 'string'
    },
    membershipType: {
      description: 'The platform identifier the account has been made on',
      type: 'integer',
      enum: [1, 2, 3, 5]
    },
    membershipId: {
      description: 'The unique identifier for the account',
      type: 'string'
    },
    isBungieName: {
      description: 'Indicates if the displayName is in the new Bungie name format',
      type: 'boolean'
    },
    score: {
      description: 'Triumph/grimoire score',
      type: 'integer'
    },
    legacyScore: {
      description: 'Triumph score for old vaulted triumphs',
      type: 'integer'
    },
    seals: {
      description: 'The number of seals (titles) the player has acquired',
      type: 'integer'
    },
    dateLastPlayed: {
      description: 'The last time the account has started an activity',
      type: 'string'
    },
    crossSaveActive: {
      description: 'Indicates if the account has cross save activated and is the primary account',
      type: 'boolean'
    },
    currentlyPlaying: {
      description: 'The name of the current activity for the account',
      type: 'string'
    },
    currentlyPlayingMode: {
      description: 'The name of the current activity mode for the account',
      type: 'string'
    }
  }
}

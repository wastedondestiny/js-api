module.exports = {
  type: 'object',
  properties: {
    message: {
      description: 'The message describing the error',
      type: 'string'
    }
  }
}

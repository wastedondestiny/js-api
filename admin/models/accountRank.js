module.exports = {
  type: 'object',
  properties: {
    rank: {
      description: 'The percentile rank or the position in the leaderboard',
      type: 'integer'
    },
    isTop: {
      description: 'Whether the rank is a top position or a percentile rank',
      type: 'boolean'
    }
  }
}

module.exports = {
  type: 'object',
  properties: {
    players: {
      description: 'The players on the leaderboard page',
      type: 'object',
      properties: {
        gameVersion: {
          description: 'The version of the game the account is on (Destiny 1 or Destiny 2)',
          type: 'integer',
          enum: [1, 2]
        },
        displayName: {
          description: 'The name displayed in the game, or the platform name if not found',
          type: 'string'
        },
        membershipType: {
          description: 'The platform identifier the account has been made on',
          type: 'integer',
          enum: [1, 2, 3, 5]
        },
        membershipId: {
          description: 'The unique identifier for the account',
          type: 'string'
        },
        value: {
          description: 'The requested value to sort by',
          type: 'integer'
        }
      }
    },
    page: {
      description: 'The current page number',
      type: 'integer'
    },
    count: {
      description: 'The number of players on this page',
      type: 'integer'
    },
    totalPlayers: {
      description: 'The number of players tracked in total',
      type: 'integer'
    },
    totalPlayersPlatform: {
      description: 'The number of players tracked on this membershipType and gameVersion',
      type: 'integer'
    }
  }
}

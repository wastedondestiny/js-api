module.exports = {
  type: 'object',
  properties: {
    html: {
      description: 'An HTML string stating the issue in a few words',
      type: 'string'
    },
    link: {
      description: 'A link to the tweet or article describing the issue',
      type: 'string'
    }
  }
}

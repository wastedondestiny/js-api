module.exports = {
  type: 'object',
  properties: {
    lastThirtyDays: {
      description: 'A dictionary of all the time spent on activities in the last thirty days, grouped by day',
      type: 'object'
    },
    breakdown: {
      description: 'A dictionary of all the time spent on activities on this account, grouped by type',
      type: 'object',
      properties: {
        strike: {
          description: 'Time spent on strike/nightfall activities',
          type: 'integer'
        },
        story: {
          description: 'Time spent on story/patrol/seasonal activities',
          type: 'integer'
        },
        crucible: {
          description: 'Time spent on crucible/competitive activities',
          type: 'integer'
        },
        gambit: {
          description: 'Time spent on gambit activities',
          type: 'integer'
        },
        raid: {
          description: 'Time spent on raid/dungeon activities',
          type: 'integer'
        }
      }
    }
  }
}

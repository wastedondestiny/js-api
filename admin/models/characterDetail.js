module.exports = {
  type: 'object',
  properties: {
    characterId: {
      description: 'The unique identifier for the character',
      type: 'string'
    },
    deleted: {
      description: 'Indicates if the character has been deleted',
      type: 'boolean'
    },
    pvpKda: {
      description: 'KA/D ratio on this character for PvP activities',
      type: 'number'
    },
    pveKda: {
      description: 'KA/D ratio on this character for PvE activities',
      type: 'number'
    },
    timePlayed: {
      description: 'Total time actively played',
      type: 'integer'
    },
    timeAfk: {
      description: 'Total time passively played (social spaces and orbit)',
      type: 'integer'
    },
    race: {
      description: 'The race for the character',
      type: 'string'
    },
    gender: {
      description: 'The gender for the character',
      type: 'string'
    },
    class: {
      description: 'The class for the character',
      type: 'string'
    },
    emblemPath: {
      description: 'The emblem filename',
      type: 'string'
    },
    backgroundPath: {
      description: 'The emblem background filename',
      type: 'string'
    },
    power: {
      description: 'The power level for the character',
      type: 'integer'
    },
    level: {
      description: 'The season pass level',
      type: 'integer'
    },
    legend: {
      description: 'The artifact power bonus',
      type: 'integer'
    },
    currentlyPlaying: {
      description: 'The activity the character is currently playing',
      type: 'string'
    },
    currentlyPlayingMode: {
      description: 'The activity mode the character is currently playing',
      type: 'string'
    }
  }
}

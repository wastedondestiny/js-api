module.exports = {
  type: 'object',
  properties: {
    id: {
      description: 'The season number',
      type: 'integer'
    },
    name: {
      description: 'The season name',
      type: 'string'
    },
    rank: {
      description: 'The rank achieved at the end of this season by the account',
      type: 'string'
    }
  }
}

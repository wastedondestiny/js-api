const openapi = require('@wesleytodd/openapi')
const characterDetailSchema = require('../admin/models/characterDetail')
const destinyAccountSchema = require('../admin/models/destinyAccount')
const activityDetailSchema = require('../admin/models/activityDetail')
const seasonDetailSchema = require('../admin/models/seasonDetail')
const globalAlertSchema = require('../admin/models/globalAlert')
const leaderboardPageSchema = require('./models/leaderboardPage')
const accountRankSchema = require('./models/accountRank')
const errorModelSchema = require('./models/errorModel')
const { version } = require('../package.json')

const servers = []

if (process.env.NODE_ENV !== 'production') {
  servers.push({
    url: 'http://localhost:3000',
    description: 'Development server'
  })
}

servers.push({
  url: 'https://api.wastedondestiny.com',
  description: 'Production server'
})

const oapi = openapi({
  openapi: '3.0.0',
  info: {
    title: 'Wasted on Destiny API',
    description: 'Full documentation for the API for Time Wasted on Destiny (https://wastedondestiny.com)',
    version,
  },
  servers,
  tags: [
    {
      name: 'destiny',
      description: 'Destiny-related endpoints (game status, accounts, characters, stats)'
    },
    {
      name: 'leaderboard',
      description: 'Leaderboard-related endpoints (ranking, leaderboard)'
    }
  ]
})

oapi.schema('CharacterDetail', characterDetailSchema)
oapi.schema('DestinyAccount', destinyAccountSchema)
oapi.schema('ActivityDetail', activityDetailSchema)
oapi.schema('SeasonDetail', seasonDetailSchema)
oapi.schema('GlobalAlert', globalAlertSchema)
oapi.schema('LeaderboardPage', leaderboardPageSchema)
oapi.schema('AccountRank', accountRankSchema)
oapi.schema('ErrorModel', errorModelSchema)

module.exports = oapi

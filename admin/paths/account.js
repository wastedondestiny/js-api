module.exports = {
  description: 'Get Destiny accounts for a given membership ID',
  tags: ['destiny'],
  parameters: [
    {
      in: 'query',
      name: 'language',
      schema: {
        type: 'string'
      }
    }
  ],
  responses: {
    200: {
      content: {
        'application/json': {
          schema: {
            type: 'array',
            items: {
              $ref: '#/components/schemas/DestinyAccount'
            }
          }
        }
      }
    },
    404: {
      content: {
        'application/json': {
          schema: {
            $ref: '#/components/schemas/ErrorModel'
          }
        }
      }
    }
  }
}

module.exports = {
  description: 'Get global alerts set by Bungie regarding live services status and outages',
  tags: ['destiny'],
  parameters: [
    {
      in: 'query',
      name: 'language',
      schema: {
        type: 'string'
      }
    }
  ],
  responses: {
    200: {
      content: {
        'application/json': {
          schema: {
            type: 'array',
            items: {
              $ref: '#/components/schemas/GlobalAlert'
            }
          }
        }
      }
    }
  }
}

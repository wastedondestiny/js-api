module.exports = {
  description: 'Get Destiny account rank on the leaderboard',
  tags: ['leaderboard'],
  responses: {
    200: {
      content: {
        'application/json': {
          schema: {
            type: 'array',
            items: {
              $ref: '#/components/schemas/AccountRank'
            }
          }
        }
      }
    },
    404: {
      content: {
        'application/json': {
          schema: {
            $ref: '#/components/schemas/ErrorModel'
          }
        }
      }
    }
  }
}

module.exports = {
  description: 'Get Destiny activity breakdown for a given account',
  tags: ['destiny'],
  responses: {
    200: {
      content: {
        'application/json': {
          schema: {
            $ref: '#/components/schemas/ActivityDetail'
          }
        }
      }
    },
    404: {
      content: {
        'application/json': {
          schema: {
            $ref: '#/components/schemas/ErrorModel'
          }
        }
      }
    }
  }
}

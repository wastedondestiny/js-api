module.exports = {
  description: 'Get Destiny characters information for a given account',
  tags: ['destiny'],
  parameters: [
    {
      in: 'query',
      name: 'language',
      schema: {
        type: 'string'
      }
    }
  ],
  responses: {
    200: {
      content: {
        'application/json': {
          schema: {
            type: 'array',
            items: {
              $ref: '#/components/schemas/CharacterDetail'
            }
          }
        }
      }
    },
    404: {
      content: {
        'application/json': {
          schema: {
            $ref: '#/components/schemas/ErrorModel'
          }
        }
      }
    }
  }
}

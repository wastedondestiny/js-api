module.exports = {
  description: 'Get leaderboard pages',
  tags: ['leaderboard'],
  parameters: [
    {
      in: 'query',
      name: 'sorting',
      schema: {
        type: 'string'
      }
    }
  ],
  responses: {
    200: {
      content: {
        'application/json': {
          schema: {
            type: 'array',
            items: {
              $ref: '#/components/schemas/LeaderboardPage'
            }
          }
        }
      }
    },
    404: {
      content: {
        'application/json': {
          schema: {
            $ref: '#/components/schemas/ErrorModel'
          }
        }
      }
    }
  }
}

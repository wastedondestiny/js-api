const getAccount = require('../repositories/destiny/getAccount')
const getProfile = require('../repositories/destiny2/getProfile')
const getAccountHistoricalStatsD1 = require('../repositories/destiny/getAccountHistoricalStats')
const getAccountHistoricalStatsD2 = require('../repositories/destiny2/getAccountHistoricalStats')
const {
  getDestiny1CharacterData, getDestiny1CharacterStats, getDestiny1CharacterModels,
  getDestiny2CharacterData, getDestiny2CharacterProgressions, getDestiny2CharacterStats, getDestiny2CharacterModels, getCurrentSeason
} = require('./factories')
const seasonsList = require('../manifest/seasons.json')
const titlesList = require('../manifest/records.json')

module.exports = {
  getDestiny1Characters: async (membershipType, membershipId) => {
    if (!membershipType || !membershipId) {
      throw new Error('MembershipType and membershipId must be set')
    }

    const characterData = getDestiny1CharacterData(await getAccount(membershipType, membershipId))
    const characterStats = getDestiny1CharacterStats(await getAccountHistoricalStatsD1(membershipType, membershipId))
    const mappedCharacters = characterStats.map(x => ({
      data: characterData.find(y => y.characterId === x.characterId),
      stats: x
    }))
    const characters = getDestiny1CharacterModels(mappedCharacters)

    return characters
  },
  getDestiny2Characters: async (membershipType, membershipId, locale) => {
    if (!membershipType || !membershipId) {
      throw new Error('MembershipType and membershipId must be set')
    }

    const profile = await getProfile(membershipType, membershipId, 2)
    const characterData = getDestiny2CharacterData(profile, titlesList, locale)
    const currentSeason = getCurrentSeason(profile, seasonsList)
    const characterProgressions = getDestiny2CharacterProgressions(profile, currentSeason)
    const characterStats = getDestiny2CharacterStats(await getAccountHistoricalStatsD2(membershipType, membershipId))
    const mappedCharacters = characterStats.map(x => ({
      data: characterData.find(y => y.characterId === x.characterId),
      progression: characterProgressions.find(y => y.characterId === x.characterId),
      stats: x,
      triumph: (profile.profileRecords.data?.legacyScore ?? 0) + (profile.profileRecords.data?.score ?? 0),
      seals: Object.keys(titlesList).reduce((acc, x) => acc + (profile.profileRecords.data?.records[x]?.state > 64), 0)
    }))
    const characters = getDestiny2CharacterModels(mappedCharacters)

    return characters
  }
}

const getActivityHistory = require('../repositories/destiny2/getActivityHistory')
const { getActivities } = require('../utils/factories')

module.exports = {
  getActivityHistory: (membershipType, membershipId, characterId) => new Promise(async (resolve, reject) => {
    let allActivities = []
    let page = 0

    while (true) {
      try {
        const activitiesPage = await getActivityHistory(membershipType, membershipId, characterId, page++)
        const activities = getActivities(activitiesPage)
        
        if (activities?.length) {
          allActivities = [...allActivities, ...activities]
        } else {
          return resolve(allActivities)
        }
      } catch (e) {
        return reject(e)
      }
    }
  })
}

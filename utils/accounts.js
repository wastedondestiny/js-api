const getMembershipsById = require('../repositories/user/getMembershipsById')
const getInvalidLinkedProfiles = require('../repositories/destiny2/getInvalidLinkedProfiles')
const getProfile = require('../repositories/destiny2/getProfile')
const getAccount = require('../repositories/destiny/getAccount')
const { getDestinyAccountModels, getDestiny1MembershipModel, getDestiny2MembershipModel, getCurrentActivity, getInvalidAccountModels, getGroupModel } = require('./factories')
const activityList = require('../manifest/activities.json')
const titlesList = require('../manifest/records.json')
const seasonsList = require('../manifest/seasons.json')
const getGroupForMember = require('../repositories/group/getGroupForMember')

module.exports = {
  getAccountsForMembershipId: async (membershipId, locale) => {
    if (!membershipId) {
      throw new Error('MembershipId must be set')
    }

    const membershipsById = await getMembershipsById(membershipId)
    const destinyAccounts = getDestinyAccountModels(membershipsById)
    const invalidLinkedProfiles = await getInvalidLinkedProfiles(membershipId)
    const invalidDestinyAccounts = getInvalidAccountModels(invalidLinkedProfiles)
    const destiny1Accounts = new Map()
    const destiny2Accounts = new Map()

    if (!destinyAccounts.length) {
      throw new Error('No memberships found')
    }

    for (const membership of destinyAccounts) {
      const { membershipType, membershipId } = membership
      const group = await getGroupForMember(membershipType, membershipId)
      const groupModel = getGroupModel(group?.results?.[0]?.group)

      if (!invalidDestinyAccounts.find(x => x.membershipId === membershipId) && !destiny2Accounts.has(membershipId)) {
        try {
          // Retrieve data about Destiny 2 profile
          const destiny2Profile = await getProfile(membershipType, membershipId, 1)
          const currentActivity = getCurrentActivity(destiny2Profile, activityList, locale)
          const season = seasonsList[destiny2Profile.profile.data.currentSeasonHash]
          destiny2Accounts.set(membershipId, getDestiny2MembershipModel(destiny2Profile, membership, groupModel, currentActivity, season?.maxTriumphScore, titlesList, locale))
        } catch (_) {
          destiny2Accounts.set(membershipId, null)
        }
      }

      if ([1, 2].includes(membershipType) && !destiny1Accounts.has(membershipId)) {
        try {
          // Retrieve data about Destiny 1 account
          const destiny1Account = await getAccount(membershipType, membershipId)
          destiny1Accounts.set(membershipId, getDestiny1MembershipModel(destiny1Account, membership, groupModel))
        } catch (_) {
          destiny1Accounts.set(membershipId, null)
        }
      }
    }

    if (!destiny1Accounts.size && !destiny2Accounts.size) {
      throw new Error('No accounts found')
    }

    return [
      ...Array.from(destiny2Accounts.values()).filter(Boolean),
      ...Array.from(destiny1Accounts.values()).filter(Boolean)
    ]
  }
}

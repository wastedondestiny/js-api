module.exports = {
  isSteam64Format: steam64 => !isNaN(+steam64) && (Math.log(+steam64) * Math.LOG10E + 1 | 0) == 17,
  unique: (array, key = null) => key ? [...new Map(array.map(x => [x[key], x])).values()] : [...new Set(array)],
  dateRangeDays: (dateStart, dateEnd) => { const dates = []; for (let date = dateStart; date <= dateEnd; date.setDate(date.getDate() + 1)) dates.push(new Date(date)); return dates },
  dateYMDFormat: date => (new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()))).toISOString().slice(0, 10),
  sameDate: (a, b) => a.getFullYear() === b.getFullYear() && a.getMonth() === b.getMonth() && a.getDate() === b.getDate(),
  reverseMap: map => Object.keys(map).reduce((acc, key) => map[key].reduce((a, num) => ({ ...a, [num]: key }), acc), {}),
  parse: json => { try { return JSON.parse(json) } catch (e) {} }
}

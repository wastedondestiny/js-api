const fs = require('fs')
const os = require('os')
const path = require('path')

const envFilePath = path.resolve(path.dirname(require.main.filename), '.env')
const readEnvVars = () => fs.readFileSync(envFilePath, 'utf-8').split(os.EOL)

const getEnvValue = key => {
  const matchedLine = readEnvVars().find(line => line.split('=')[0] === key)
  return matchedLine ? matchedLine.split('=')[1] : null
}

const setEnvValue = (key, value) => {
  const envVars = readEnvVars()
  const targetLine = envVars.find(line => line.split('=')[0] === key)

  if (targetLine) {
    const targetLineIndex = envVars.indexOf(targetLine)
    envVars.splice(targetLineIndex, 1, `${key}=${value}`)
  } else {
    envVars.push(`${key}=${value}`)
  }

  fs.writeFileSync(envFilePath, envVars.join(os.EOL))
}

module.exports = {
  getEnvValue,
  setEnvValue
}

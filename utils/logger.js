const Bugsnag = require('@bugsnag/js')
const BugsnagPluginExpress = require('@bugsnag/plugin-express')
const revision = require('child_process')
  .execSync('git rev-parse --short HEAD')
  .toString().trim()

let plugin = {
  requestHandler: (req, res, next) => {
    next()
  },
  errorHandler: (req, res, next) => {
    next()
  }
}
let logger = {
  notify: console.log
}

if (process.env.BUGSNAG_API_KEY) {
  Bugsnag.start({
    apiKey: process.env.BUGSNAG_API_KEY,
    plugins: [BugsnagPluginExpress],
    appVersion: revision
  })

  plugin = Bugsnag.getPlugin('express')
  logger = Bugsnag
}

module.exports = {
  plugin,
  logger
}
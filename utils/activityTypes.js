const { reverseMap } = require('.')
const destiny2ActivityMode = require('../enums/destiny2ActivityMode')

const types = reverseMap({
  story: [
    destiny2ActivityMode.story,
    destiny2ActivityMode.patrol,
    destiny2ActivityMode.heroicAdventure,
    destiny2ActivityMode.blackArmoryRun,
    destiny2ActivityMode.menagerie,
    destiny2ActivityMode.vexOffensive,
    destiny2ActivityMode.sundial,
    destiny2ActivityMode.dares,
    destiny2ActivityMode.offensive,
    destiny2ActivityMode.lostSector
  ],
  strike: [
    destiny2ActivityMode.strike,
    destiny2ActivityMode.nightfall,
    destiny2ActivityMode.heroicNightfall,
    destiny2ActivityMode.allStrikes,
    destiny2ActivityMode.scoredNightfall,
    destiny2ActivityMode.scoredHeroicNightfall,
    destiny2ActivityMode.nightmareHunt
  ],
  raid: [
    destiny2ActivityMode.dungeon,
    destiny2ActivityMode.raid
  ],
  gambit: [
    destiny2ActivityMode.gambit,
    destiny2ActivityMode.allPvECompetitive,
    destiny2ActivityMode.gambitPrime,
    destiny2ActivityMode.reckoning,
    destiny2ActivityMode.privateGambit
  ],
  crucible: [
    destiny2ActivityMode.allPvP,
    destiny2ActivityMode.control,
    destiny2ActivityMode.clash,
    destiny2ActivityMode.crimsonDoubles,
    destiny2ActivityMode.ironBanner,
    destiny2ActivityMode.allMayhem,
    destiny2ActivityMode.supremacy,
    destiny2ActivityMode.privateMatchesAll,
    destiny2ActivityMode.survival,
    destiny2ActivityMode.countdown,
    destiny2ActivityMode.trialsOfTheNine,
    destiny2ActivityMode.trialsCountdown,
    destiny2ActivityMode.trialsSurvival,
    destiny2ActivityMode.ironBannerControl,
    destiny2ActivityMode.ironBannerClash,
    destiny2ActivityMode.ironBannerSupremacy,
    destiny2ActivityMode.rumble,
    destiny2ActivityMode.allDoubles,
    destiny2ActivityMode.doubles,
    destiny2ActivityMode.privateMatchesClash,
    destiny2ActivityMode.privateMatchesControl,
    destiny2ActivityMode.privateMatchesSupremacy,
    destiny2ActivityMode.privateMatchesCountdown,
    destiny2ActivityMode.privateMatchesSurvival,
    destiny2ActivityMode.privateMatchesMayhem,
    destiny2ActivityMode.privateMatchesRumble,
    destiny2ActivityMode.showdown,
    destiny2ActivityMode.lockdown,
    destiny2ActivityMode.scorched,
    destiny2ActivityMode.scorchedTeam,
    destiny2ActivityMode.breakthrough,
    destiny2ActivityMode.salvage,
    destiny2ActivityMode.ironBannerSalvage,
    destiny2ActivityMode.pvPCompetitive,
    destiny2ActivityMode.pvPQuickplay,
    destiny2ActivityMode.clashQuickplay,
    destiny2ActivityMode.clashCompetitive,
    destiny2ActivityMode.controlQuickplay,
    destiny2ActivityMode.controlCompetitive,
    destiny2ActivityMode.elimination,
    destiny2ActivityMode.momentum,
    destiny2ActivityMode.trialsOfOsiris,
    destiny2ActivityMode.rift,
    destiny2ActivityMode.zoneControl,
    destiny2ActivityMode.ironBannerRift,
    destiny2ActivityMode.ironBannerZoneControl
  ]
})

module.exports = activityMode => types[activityMode]
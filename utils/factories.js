const destiny1Class = require('../enums/destiny1Class')
const destiny1Race = require('../enums/destiny1Race')
const destiny1Gender = require('../enums/destiny1Gender')
const destiny2Class = require('../enums/destiny2Class')
const destiny2Race = require('../enums/destiny2Race')
const destiny2Gender = require('../enums/destiny2Gender')
const getActivityType = require('./activityTypes')
const { dateRangeDays, dateYMDFormat, sameDate } = require('.')
const destiny2ActivityMode = require('../enums/destiny2ActivityMode')
const { logger } = require('./logger')

module.exports = {
  getCurrentActivity (entity, activityList, locale) {
    const activities = Object.values(entity.characterActivities?.data ?? {})
    if (!activities.length) return null
    const currentActivity = activities.sort((a, b) => new Date(b.dateActivityStarted) - new Date(a.dateActivityStarted))[0]
    if (!currentActivity || currentActivity.currentActivityHash === 0) return null
    return {
      activity: activityList[currentActivity.currentActivityHash]?.[locale]?.name || 'In orbit',
      activityMode: activityList[currentActivity.currentActivityHash]?.[locale]?.mode || ''
    }
  },
  getCurrentSeason (entity, seasons) {
    const currentSeason = seasons[entity.profile?.data?.currentSeasonHash ?? 0]
    if (!currentSeason) return null
    return {
      reward: currentSeason.rewardProgressionHash,
      prestige: currentSeason.prestigeProgressionHash
    }
  },
  getDestiny1CharacterData (entity) {
    const characters = []

    for (const character of entity.data?.characters) {
      characters.push({
        characterId: character.characterBase.characterId,
        backgroundPath: character.backgroundPath,
        emblemPath: character.emblemPath,
        power: character.characterBase.powerLevel,
        level: character.characterLevel,
        class: destiny1Class[character.characterBase.classType],
        race: destiny1Race[character.characterBase.raceHash],
        gender: destiny1Gender[character.characterBase.genderType]
      })
    }

    return characters
  },
  getDestiny1CharacterStats (entity) {
    const characters = []

    for (const character of entity.characters) {
      characters.push({
        characterId: character.characterId,
        deleted: character.deleted,
        timePlayed: character.merged?.allTime?.secondsPlayed?.basic?.value ?? 0,
        pvpKda: character.results?.allPvP?.allTime?.killsDeathsAssists?.basic?.value ?? 0.0,
        pveKda: character.results?.allPvE?.allTime?.killsDeathsAssists?.basic?.value ?? 0.0
      })
    }

    return characters
  },
  getDestiny2CharacterData (entity, titles, locale) {
    if (!entity.characters?.data) return []
    const characters = []

    for (const character of Object.values(entity.characters.data)) {
      const gildedInfo = entity.profileRecords?.data?.records?.[titles[character.titleRecordHash]?.gilded]

      characters.push({
        characterId: character.characterId,
        backgroundPath: character.emblemBackgroundPath,
        emblemPath: character.emblemPath,
        power: character.light,
        title: titles[character.titleRecordHash]?.[locale],
        gilded: gildedInfo?.objectives[0].complete,
        gildedCount: gildedInfo?.completedCount,
        timeAfk: character.minutesPlayedTotal * 60,
        class: destiny2Class[character.classType],
        race: destiny2Race[character.raceType],
        gender: destiny2Gender[character.genderType]
      })
    }

    return characters
  },
  getDestiny2CharacterProgressions (entity, season) {
    if (!entity.characterProgressions?.data) return []
    const characters = []

    for (const [key, character] of Object.entries(entity.characterProgressions.data)) {
      const level = character.progressions?.[season?.reward]?.level || 1
      const prestige = character.progressions?.[season?.prestige]?.level || 0
      characters.push({
        characterId: key,
        level: level + prestige,
        legend: entity.profileProgression?.data?.seasonalArtifact?.powerBonus
      })
    }

    return characters
  },
  getDestiny2CharacterStats (entity) {
    if (!entity.characters) return []
    const characters = []

    for (const character of Object.values(entity.characters)) {
      characters.push({
        characterId: character.characterId,
        deleted: character.deleted,
        timePlayed: character.merged?.allTime?.secondsPlayed?.basic?.value ?? 0,
        pvpKda: character.results?.allPvP?.allTime?.killsDeathsAssists?.basic?.value ?? 0.0,
        pveKda: character.results?.allPvE?.allTime?.killsDeathsAssists?.basic?.value ?? 0.0
      })
    }

    return characters
  },
  getDestiny2SeasonBreakdown (progressions, seasons, locale) {
    const breakdown = []

    if (progressions?.[2030054750]?.currentProgress) {
      breakdown.push({
        id: 1,
        name: 'Legacy',
        rank: progressions?.[2030054750]?.level || 0
      })
    } else {
      breakdown.push({
        id: 1,
        name: 'Legacy',
        rank: '-'
      })
    }

    for (const season of Object.values(seasons)) {
      if (progressions?.[season?.rewardProgressionHash]?.currentProgress) {
        const level = progressions?.[season?.rewardProgressionHash]?.level || 1
        const prestige = progressions?.[season?.prestigeProgressionHash]?.level || 0
        breakdown.push({
          id: season.id,
          name: season.name[locale],
          rank: level + prestige
        })
      } else {
        breakdown.push({
          id: season.id,
          name: season.name[locale],
          rank:  '-'
        })
      }
    }
    
    return breakdown
  },
  getCharacterIds (entity) {
    if (!entity.characters?.data) return []
    return Object.keys(entity.characters.data)
  },
  getActivities (entity) {
    if (!entity.activities) return []
    const activities = []

    for (const activity of entity.activities) {
      activities.push({
        id: activity.activityDetails.instanceId,
        mode: (activity.activityDetails.mode || activity.activityDetails.modes[0]) ?? destiny2ActivityMode.privateGambit,
        period: new Date(activity.period),
        timePlayed: +activity.values?.timePlayedSeconds?.basic?.value
      })
    }

    return activities
  },
  getDialogModels (entities) {
    const model = []

    for (const dialog of entities) {
      model.push({
        id: dialog.id,
        title: dialog.title,
        text: dialog.text,
        image: dialog.image
      })
    }

    return model
  },
  getGlobalAlertModels (entities, settings) {
    const alerts = []
    
    for (const alert of entities) {
      alerts.push({
        html: alert.AlertHtml,
        link: alert.AlertLink
      })
    }

    if (settings?.systems?.D2Profiles?.enabled === false || settings?.systems?.D2Characters?.enabled === false) {
      alerts.push({
        html: 'The API is currently down for maintenance. Stay tuned on Twitter for further information.',
        link: 'https://twitter.com/BungieHelp'
      })
    }

    return alerts
  },
  getDestinyAccountModels (entities) {
    const model = []

    for (const account of entities) {
      model.push({
        membershipType: account.membershipType,
        membershipId: account.membershipId,
        displayName: account.displayName,
        bungieDisplayName: account.bungieGlobalDisplayName ? `${account.bungieGlobalDisplayName}#${account.bungieGlobalDisplayNameCode.toString().padStart(4, '0')}` : null
      })
    }

    return model
  },
  getInvalidAccountModels (entities) {
    const model = []

    for (const account of entities) {
      model.push({
        membershipType: account.infoCard.membershipType,
        membershipId: account.infoCard.membershipId
      })
    }

    return model
  },
  getDestiny2MembershipModel (entity, membership, group, currentActivity, maxTriumphScore, titlesList) {
    return {
      gameVersion: 2,
      displayName: membership.bungieDisplayName ?? membership.displayName,
      membershipType: membership.membershipType,
      membershipId: membership.membershipId,
      isBungieName: !!membership.bungieDisplayName,
      guardianRank: entity.profile.data?.currentGuardianRank || undefined,
      score: entity.profileRecords.data?.score ?? 0,
      legacyScore: entity.profileRecords.data?.legacyScore ?? 0,
      maxScore: maxTriumphScore,
      seals: Object.keys(titlesList).reduce((acc, x) => acc + (entity.profileRecords.data?.records[x]?.state >= 64), 0),
      dateLastPlayed: !currentActivity ? entity.profile.data?.dateLastPlayed : null,
      crossSaveActive: entity.profile.data?.userInfo?.crossSaveOverride === membership.membershipType,
      currentlyPlaying: currentActivity?.activity,
      currentlyPlayingMode: currentActivity?.activityMode,
      clan: group
    }
  },
  getDestiny1MembershipModel (entity, membership, group) {
    return {
      gameVersion: 1,
      displayName: membership.displayName,
      membershipType: membership.membershipType,
      membershipId: membership.membershipId,
      isBungieName: false,
      score: entity.data.grimoireScore,
      dateLastPlayed: entity.data.dateLastPlayed,
      clan: group
    }
  },
  getDestiny1CharacterModels (characters) {
    const model = []

    for (const character of characters) {
      model.push({
        characterId: character.stats.characterId,
        deleted: character.stats.deleted,
        pvpKda: character.stats.pvpKda,
        pveKda: character.stats.pveKda,
        timePlayed: character.stats.timePlayed,
        race: character.data?.race,
        gender: character.data?.gender,
        class: character.data?.class,
        emblemPath: character.data?.emblemPath,
        backgroundPath: character.data?.backgroundPath,
        power: character.data?.power,
        level: character.data?.level
      })
    }

    return model
  },
  getDestiny2CharacterModels (characters) {
    const model = []

    for (const character of characters) {
      model.push({
        characterId: character.stats.characterId,
        deleted: character.stats.deleted,
        pvpKda: character.stats.pvpKda,
        pveKda: character.stats.pveKda,
        timePlayed: character.stats.timePlayed,
        race: character.data?.race,
        gender: character.data?.gender,
        class: character.data?.class,
        emblemPath: character.data?.emblemPath,
        backgroundPath: character.data?.backgroundPath,
        power: character.data?.power,
        title: character.data?.title,
        gilded: character.data?.gilded,
        gildedCount: character.data?.gildedCount,
        timeAfk: character.data?.timeAfk - character.stats.timePlayed,
        triumph: character.triumph,
        seals: character.seals,
        level: character.progression?.level,
        legend: character.progression?.legend
      })
    }

    return model
  },
  getLastThirtyDaysActivitiesModel (activities, today, thirtyDaysAgo) {
    const days = {}
    const lastThirtyDays = dateRangeDays(thirtyDaysAgo, today)

    for (const date of lastThirtyDays) {
      const day = dateYMDFormat(date)
      let timePlayed = 0

      for (const activity of activities) {
        if (sameDate(activity.period, date)) {
          timePlayed += activity.timePlayed
        }
      }

      days[day] = timePlayed
    }

    return days
  },
  getActivityBreakdownModel (activities) {
    const model = {}

    for (const activity of activities) {
      const type = getActivityType(activity.mode)
      if (!type) logger.notify(`Missing activity type`, event => {
        event.addMetaData('mode', activity.mode)
      })
      model[type] = (model[type] || 0) + activity.timePlayed
    }

    return model
  },
  getSupporterModels (supporters) {
    const model = []

    for (const supporter of supporters) {
      model.push({
        name: supporter.name,
        membershipId: supporter.linkedMembershipIds?.[0],
        tier: supporter.tier.substring(0, supporter.tier.indexOf('(') - 1)
      })
    }

    return model
  },
  getGroupModel (group) {
    if (!group) return null

    return {
      id: group.groupId,
      name: group.name,
      motto: group.motto
    }
  },
  getClanModel (group, accounts) {
    return {
      id: group.detail.groupId,
      name: group.detail.name,
      motto: group.detail.motto,
      about: group.detail.about,
      accounts
    }
  }
}

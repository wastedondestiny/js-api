module.exports = database => (req, res, next) => {
  req.database = database
  next()
}

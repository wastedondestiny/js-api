const revision = require('child_process')
  .execSync('git rev-parse --short HEAD')
  .toString().trim()

module.exports = (req, res, next) => {
  res.set('X-Version', revision)
  next()
}

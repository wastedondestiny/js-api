module.exports = age => (req, res, next) => {
  res.header('Cache-Control', `public, max-age=${age}`)
  next()
}
